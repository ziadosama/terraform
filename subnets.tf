#Public and Private subnets

resource "aws_subnet" "Pub1" {
  vpc_id     = aws_vpc.Ziad-VPC.id
  cidr_block = var.pub1-cidr
  tags = {
    Name = join("", [var.env, "-subnet-pub1-", var.region])
  }
  availability_zone = var.az1
}

resource "aws_subnet" "Pub2" {
  vpc_id     = aws_vpc.Ziad-VPC.id
  cidr_block = var.pub2-cidr
  tags = {
    Name = join("", [var.env, "-subnet-pub2-", var.region])
  }
  availability_zone = var.az2
}

resource "aws_subnet" "Pri1" {
  vpc_id     = aws_vpc.Ziad-VPC.id
  cidr_block = var.pri1-cidr
  tags = {
    Name = join("", [var.env, "-subnet-pri1-", var.region])
  }
  availability_zone = var.az1
}

resource "aws_subnet" "Pri2" {
  vpc_id     = aws_vpc.Ziad-VPC.id
  cidr_block = var.pri2-cidr
  tags = {
    Name = join("", [var.env, "-subnet-pri2-", var.region])
  }
  availability_zone = var.az2
}
