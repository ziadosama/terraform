# ALB, ALB listener and Target group

resource "aws_lb" "app-alb" {
  name               = join("", [var.env, "-app-alb-", var.region])
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.app-alb-sg.id]
  subnets            = [aws_subnet.Pub1.id, aws_subnet.Pub2.id]
  idle_timeout       = "300"

  access_logs {
    bucket  = aws_s3_bucket.s3-alb.bucket
    prefix  = join("", [var.env, "-alb-logs-", var.region])
    enabled = true
  }

  tags = {
    Name        = join("", [var.env, "-app-alb-", var.region])
    Environment = var.env
    Region      = var.region
  }
}

resource "aws_lb_target_group" "app-alb-tg" {
  name                 = join("", [var.env, "-app-alb-tg-", var.region])
  port                 = 8080
  protocol             = "HTTP"
  target_type          = "ip"
  vpc_id               = aws_vpc.Ziad-VPC.id
  deregistration_delay = 60
  lifecycle {
    create_before_destroy = true
  }
  health_check {
    healthy_threshold   = "5"
    unhealthy_threshold = "3"
    timeout             = "30"
    path                = "/"
    interval            = "35"
    port                = "8080"
    matcher             = "200-401"
    protocol            = "HTTP"
  }
}

resource "aws_lb_listener" "app-alb-http" {
  load_balancer_arn = aws_lb.app-alb.arn
  port              = "80"
  protocol          = "HTTP"
  default_action {
    target_group_arn = aws_lb_target_group.app-alb-tg.arn
    type             = "forward"
  }
}
