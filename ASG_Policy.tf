# Autoscaling policies based on memory and CPU utilization

resource "aws_appautoscaling_policy" "ecs-policy-memory" {
  name               = "memory-autoscaling"
  resource_id        = aws_appautoscaling_target.app-asg.resource_id
  scalable_dimension = aws_appautoscaling_target.app-asg.scalable_dimension
  service_namespace  = aws_appautoscaling_target.app-asg.service_namespace
  policy_type        = "TargetTrackingScaling"

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageMemoryUtilization"
    }

    target_value = 80
  }
}

resource "aws_appautoscaling_policy" "ecs-policy-cpu" {
  name               = "cpu-autoscaling"
  resource_id        = aws_appautoscaling_target.app-asg.resource_id
  scalable_dimension = aws_appautoscaling_target.app-asg.scalable_dimension
  service_namespace  = aws_appautoscaling_target.app-asg.service_namespace
  policy_type        = "TargetTrackingScaling"

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageCPUUtilization"
    }

    target_value = 70
  }
}