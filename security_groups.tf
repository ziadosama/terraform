#Security groups

resource "aws_security_group" "app-sg" {
  name        = join("", [var.env, "-app-sg-", var.region])
  description = "app security group"
  vpc_id      = aws_vpc.Ziad-VPC.id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port       = 8080
    to_port         = 8080
    protocol        = "TCP"
    security_groups = [aws_security_group.app-alb-sg.id]
  }

  tags = {
    Name        = join("", [var.env, "-app-sg-", var.region])
    Environment = var.env
    Region      = var.region
  }
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_security_group" "app-alb-sg" {
  name   = join("", [var.env, "-app-alb-sg-", var.region])
  vpc_id = aws_vpc.Ziad-VPC.id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name        = join("", [var.env, "-app-alb-sg-", var.region])
    Environment = var.env
    Region      = var.region
  }
  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_security_group" "app-db-sg" {
  name   = join("", [var.env, "-app-db-sg-", var.region])
  vpc_id = aws_vpc.Ziad-VPC.id

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port       = 3306
    to_port         = 3306
    protocol        = "TCP"
    security_groups = [aws_security_group.app-sg.id]
  }
  tags = {
    Name        = join("", [var.env, "-app-db-sg-", var.region])
    Environment = var.env
    Region      = var.region
  }
  lifecycle {
    create_before_destroy = true
  }
}