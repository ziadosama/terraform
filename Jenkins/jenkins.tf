resource "aws_instance" "jenkins-master" {
  ami                    = data.aws_ami.amazon-linux-2.id
  availability_zone      = "us-east-1a"
  instance_type          = "t3.small"
  vpc_security_group_ids = [aws_security_group.jenkins-sg.id]
  key_name               = "jenkins-key"
  user_data              = file("udata/jenkins.sh")
  root_block_device {
    volume_size = "20"
    encrypted   = "true"
    volume_type = "gp3"
  }
  lifecycle {
    ignore_changes = [ami]
  }
  tags = {
    Name        = "jenkins-master"
  }
}

resource "aws_security_group" "jenkins-sg" {
  name        = "jenkins-sg"
  description = "jenkins security group"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "TCP"
    cidr_blocks = ["196.132.133.155/32"]
  }
  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "TCP"
    cidr_blocks = ["196.132.133.155/32"]
  }

  tags = {
    Name        = "jenkins-sg"
  }
}
