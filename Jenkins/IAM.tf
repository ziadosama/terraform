resource "aws_iam_user" "jenkins" {
  name = "jenkins"

  tags = {
    Name = "jenkins"
  }
}

resource "aws_iam_access_key" "jenkins" {
  user = aws_iam_user.jenkins.name
}


resource "aws_iam_user_policy_attachment" "jenkins-policy-1" {
  user       = aws_iam_user.jenkins.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryPowerUser"
}

resource "aws_iam_user_policy_attachment" "jenkins-policy-2" {
  user       = aws_iam_user.jenkins.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonECS_FullAccess"
}