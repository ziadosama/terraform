#!/usr/bin/env bash
yum install java-1.8.0-openjdk java-1.8.0-devel git wget docker -y
service docker start
hostname jenkins-master && echo "jenkins-master" > /etc/hostname
IP=$(ifconfig | grep inet | sed -n 1p | awk '{print $2}')
echo -e "$IP    jenkins-master" >> /etc/hosts

echo "Add maven"

cd /usr/local/src && wget http://www-us.apache.org/dist/maven/maven-3/3.5.4/binaries/apache-maven-3.5.4-bin.tar.gz
tar -xf apache-maven-3.5.4-bin.tar.gz
mv apache-maven-3.5.4/ apache-maven/ 