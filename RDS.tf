#Aurora serverless

resource "aws_db_subnet_group" "db-subnet" {
  name       = "main"
  subnet_ids = [aws_subnet.Pri1.id, aws_subnet.Pri2.id]

  tags = {
    Name = join("", [var.env, "-aurora-subnet-", var.region])
  }
}

resource "aws_rds_cluster" "app-db" {
  cluster_identifier      = join("", [var.env, "-app-db-", var.region])
  engine                  = "aurora-mysql"
  engine_mode             = "serverless"
  database_name           = "myauroradb"
  enable_http_endpoint    = true
  master_username         = "root"
  master_password         = "f0J78cRnc9YpNDHgK0"
  backup_retention_period = 1
  vpc_security_group_ids  = [aws_security_group.app-db-sg.id]
  db_subnet_group_name    = aws_db_subnet_group.db-subnet.id
  skip_final_snapshot     = true

  scaling_configuration {
    auto_pause               = true
    min_capacity             = var.desired-capacity
    max_capacity             = var.max-size
    seconds_until_auto_pause = 300
    timeout_action           = "ForceApplyCapacityChange"
  }
}