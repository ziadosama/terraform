# Fargate with Aurora serverless in terraform
Select workspace "terraform workspace select stage"\
Apply changes "terraform apply -var-file=Env/stage.tfvars"\
Destroy "terrafor destroy -var-file=Env/stage.tfvars"\
Jenkins infrastructure is managed separately and pipelines are stored in Jenkins/Pipeline 
