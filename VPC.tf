#VPC

resource "aws_vpc" "Ziad-VPC" {
  cidr_block           = var.vpc-cidr
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    Name = join("", [var.env, "-ziad-vpc-", var.region])
  }
}
