#App Autoscaling Group

resource "aws_appautoscaling_target" "app-asg" {
  min_capacity       = var.min-size
  max_capacity       = var.max-size
  resource_id        = join("", ["service/", aws_ecs_cluster.ecs-cluster.name, "/", aws_ecs_service.ecs-service.name])
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}
