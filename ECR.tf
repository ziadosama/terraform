#ECR repo with lifecycle

resource "aws_ecr_repository" "ecr-repo" {
  name                 = join("", [var.env, "-ecr-", var.region])
  image_tag_mutability = "MUTABLE"
}

resource "aws_ecr_lifecycle_policy" "ecr-policy" {
  repository = aws_ecr_repository.ecr-repo.name
  policy     = file("policy/ecr_policy.json")
}