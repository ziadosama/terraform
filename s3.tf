#S3 for ALB logs

resource "aws_s3_bucket" "s3-alb" {
  bucket = join("", [var.env, "-s3-alb-", var.region])
  acl    = "private"
  policy = file("policy/s3_alb.json")
}