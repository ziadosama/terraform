#IAM role and policy for ECS to access ECR

resource "aws_iam_role" "ecs-task-role" {
  name               = join("", [var.env, "-ecs-task-role-", var.region])
  assume_role_policy = file("policy/ecs_tasks.json")
  tags = {
    Name = join("", [var.env, "-ecs-task-role-", var.region])
  }
}

resource "aws_iam_role" "ecs-task-exec-role" {
  name               = join("", [var.env, "-ecs-exec-role-", var.region])
  assume_role_policy = file("policy/ecs_task_execution.json")
  tags = {
    Name = join("", [var.env, "-ecs-exec-role-", var.region])
  }
}

resource "aws_iam_role_policy_attachment" "ecs-task-execution-role-policy-attachment" {
  role       = aws_iam_role.ecs-task-exec-role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}