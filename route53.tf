#Private Route 53

resource "aws_route53_zone" "app" {
  name    = "app.com"
  vpc {
    vpc_id = aws_vpc.Ziad-VPC.id
  }
}

resource "aws_route53_record" "app-aurora" {
  zone_id = aws_route53_zone.app.zone_id
  name    = join("", [var.env, "-", var.region, "-aurora.app.com"])
  type    = "CNAME"
  ttl     = 300
  records = [aws_rds_cluster.app-db.endpoint]
}