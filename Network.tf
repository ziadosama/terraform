#VPC, public and private route tables

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.Ziad-VPC.id
  tags = {
    Name = join("", [var.env, "-ziad-igw-", var.region])
  }
}

resource "aws_eip" "ngw-ip" {
  vpc = true
}

resource "aws_nat_gateway" "ngw" {
  allocation_id = aws_eip.ngw-ip.id
  subnet_id     = aws_subnet.Pub1.id
}

resource "aws_network_acl" "all" {
  vpc_id     = aws_vpc.Ziad-VPC.id
  subnet_ids = [aws_subnet.Pub1.id, aws_subnet.Pub2.id, aws_subnet.Pri1.id, aws_subnet.Pri2.id]

  /*Outbound Rules*/
  egress {
    protocol   = "-1"
    rule_no    = 200
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  /*Inbound Rules*/
  ingress {
    protocol   = "-1"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }
  tags = {
    Name = "open acl"
  }
}

resource "aws_route_table" "public-rt" {
  vpc_id = aws_vpc.Ziad-VPC.id
  tags = {
    Name = join("", [var.env, "-public-rt-", var.region])
  }
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }
}

resource "aws_route_table" "private-rt" {
  vpc_id = aws_vpc.Ziad-VPC.id
  tags = {
    Name = join("", [var.env, "-private-rt-", var.region])
  }
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.ngw.id
  }
}

resource "aws_route_table_association" "Pub1-rt" {
  subnet_id      = aws_subnet.Pub1.id
  route_table_id = aws_route_table.public-rt.id
}

resource "aws_route_table_association" "Pub2-rt" {
  subnet_id      = aws_subnet.Pub2.id
  route_table_id = aws_route_table.public-rt.id
}

resource "aws_route_table_association" "Pri1-rt" {
  subnet_id      = aws_subnet.Pri1.id
  route_table_id = aws_route_table.private-rt.id
}

resource "aws_route_table_association" "Pri2-rt" {
  subnet_id      = aws_subnet.Pri2.id
  route_table_id = aws_route_table.private-rt.id
}