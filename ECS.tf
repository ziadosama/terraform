#ECS cluster, service, task definition and cloudwatch log group

resource "aws_ecs_cluster" "ecs-cluster" {
  name = join("", [var.env, "-ecs-cluster-", var.region])
}
resource "aws_ecs_task_definition" "ecs-td" {
  family                   = "ecs-app"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = var.container-cpu
  memory                   = var.container-memory
  execution_role_arn       = aws_iam_role.ecs-task-exec-role.arn
  task_role_arn            = aws_iam_role.ecs-task-role.arn
  container_definitions = jsonencode([
    {
      name      = join("", [var.env, "-ecs-app-", var.region])
      image     = join("", [var.account, ".dkr.ecr.", var.region, ".amazonaws.com/", var.env, "-ecr-", var.region, ":latest"])
      essential = true
      portMappings = [{
        containerPort = var.container-port
        hostPort      = var.container-port
      }]
      logConfiguration = {
        logDriver = "awslogs"
        options = {
          "awslogs-group" : "/ecs/app"
          "awslogs-region" : var.region
          "awslogs-stream-prefix" : "ecs"
        }
      }
    }
  ])
}

resource "aws_ecs_service" "ecs-service" {
  name                               = join("", [var.env, "-ecs-service-", var.region])
  launch_type                        = "FARGATE"
  cluster                            = aws_ecs_cluster.ecs-cluster.id
  task_definition                    = aws_ecs_task_definition.ecs-td.arn
  health_check_grace_period_seconds  = 60
  scheduling_strategy                = "REPLICA"
  desired_count                      = var.desired-capacity
  deployment_minimum_healthy_percent = "50"
  deployment_maximum_percent         = "200"

  network_configuration {
    security_groups  = [aws_security_group.app-sg.id]
    subnets          = [aws_subnet.Pri1.id, aws_subnet.Pri2.id]
    assign_public_ip = false
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.app-alb-tg.arn
    container_name   = join("", [var.env, "-ecs-app-", var.region])
    container_port   = var.container-port
  }
}

resource "aws_cloudwatch_log_group" "logs" {
  name              = "/ecs/app"
  retention_in_days = 1
}